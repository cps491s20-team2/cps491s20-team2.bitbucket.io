University of Dayton

Department of Computer Science

CPS 491 - Spring 2020

Dr. Phu Phung


# ROM Estimate Tool

# Team members


1.  Jack McCarthy, mccarthyj11@udayton.edu
2.  Ragan Tuffey, tuffeyr1@udayton.edu
3.  Ben Reutelshofer, reutelshoferb1@udayton.edu
4.  Dan Hutson, hutsond1@udayton.edu


# Company Mentors

Todd Irbeck, _Product Owner_; Chris Williams, _Program Manager_

Riverside Research

2640 Hibiscus Way, Beavercreek OH 45431

## Project homepage

[Homepage](https://cps491s20-team2.bitbucket.io/index.html)


# Overview

![Overview Architecture](imgs/overview.png "Overview Architecture")

Figure 1. - Overview Architecture of the proposed project.



![Use Case Diagram](imgs/use_case_diagram.png "Use Case Diagram")

Figure 2. - Use Case Diagram as of Sprint 1



## Create new ROM estimate from scope
### Actor
    * Person(s) in charge of creating a ROM
### User Story
    * As the actor, I want to begin the process of doing a ROM.
### Use Case Description  
    * A user will be able to start the program and create a ROM estimate with many different options to customize the ROM.

## Use Case 1: Create new ROM estimate from scope
**Author:** Ben Reutelshofer
**Date:** 1/21/2020
**Actors:** User           
**Scenario:** The user wants to create a ROM.
**Triggering Event:** The user has accessed the program.
**Brief Description:** The user has accessed the program and wants to begin to cuztomize their ROM.
**Preconditions:** The program is operational.
**Postconditions:** A new ROM has been started.
**Flow of Activities:**

| Actor                                                               | System                                                     |
|:--------------------------------------------------------------------|:-----------------------------------------------------------|
| 1. The user runs the program.                                       | 1.1 The system displays a welcome page.                    |
| 2. The user gains access to cuztomizing a ROM.                      | 2.1 The system begins to allow user to cuztomzie a ROM.    |

**Exceptions:**  The program is not operational or insecure.


# Project Context and Scope

For Riverside Research managers, business developers, and financial analysts who develop proposals and plan projects, the ROM tool is a financial application that estimates the costs of services and products.

Unlike spreadsheets built in Excel, our product is:
 * Focused on developing ROMs
 * Customizable to Riverside Research's operations
 * Intuitive to use

This tool is for internal use within the company to promote the time efficiency of ROM estimates when drafting contracts for existing and potential clients of Riverside Research.


# High-level Requirements

 * Ability to start a ROM from scope and NTE is intuitive
 * Ability to estimate the cost for a project (e.g. program management, labor, travel, materials)
 * Ability to view overall fee
 * Ability to manipulate ROMs to the owner's specifications (e.g. customize formatting, load/open, name, edit, copy, browse, export ROM as PDF, & add contract/personnel information, etc.)


# Technology

Languages:

 * HTML5
 * CSS
 * Javascript

Frameworks:

 * Bootstrap
 * Express

Database:

 * PouchDB

Development & Testing:

 * GoogleCloudShell
 * Linux

Deployment Environments:

 * Heroku

Due to the sensitive nature of defense contracting, actual deployment of a web application seems tenuous; however, Todd and Chris seemed comfortable with the idea of simply running the web application locally. For demonstrational purposes, we have received express permission to deploy to a cloud-based solution; this is for demonstrational purposes only and under no circumstances are we to use real data on this platform.


# Software Process Management

The team is using a Scrum-like process based on a 7 Sprint heat. Each sprint is two work weeks in length as layed out with the company sponsor in December 2019. To communicate, the team uses two solutions: primarily, Groupme for instant messaging, and Google hangout for live video correspondence. The majority of work for the team happens remotely with two in-person meetings per week.

#### Version Control Strategy:

 Using a bitbucket repo, each sprint will have its own branch which will be merged back to the master at the end of each sprint.

#### Branching Strategy:
We will divide our branches by sprint. For example, all work for sprint 1 will go in a branch titled sprint1 and then be merged to the master.

#### Company Support

 As the ROM tool is algorithmically well established, our major task is simply implementing the procedures which were once performed by hand, and our sponsors have been kind enough to elaborate extensively on their project requirements and the means by which we may approach the project. For Fall 2019, we have one meeting scheduled to plan for the work into December, and for Spring 2020, we will likely meet weekly using some combination of video conferences and in person meetings; should weekly meetings be too frequent, we can adjust as needed.

 - Include the Trello board with product backlog and sprint cycles in an overview figure and also in detail description.

## Sprint Cycles:

### Sprint 1: (1/14 - 1/28)
 * Identify initial architecture
 * Document branching strategy
 * Document version control strategy
 * Ability to start ROM from scope and NTE is intuitive
 * Ability to start ROM from just scope is intuitive

### Sprint 2: (1/28 - 2/11)
 * Identify security requirements
 * Ability to install and run application from workstation without admin permissions
 * Ability to name ROM

### Sprint 3: (2/11 - 2/25)
 * Ability to add contact information
 * Ability to add personnel information
 * Ability to estimate cost for materials
 * Ability to estiamte cost for travel

### Sprint 4: (2/25 - 3/10)
 * Ability to estimate the cost for labor
 * Ability to estimate the cost for program management
 * Ability to view overall fee
 * Investigate file storage

### Sprint 5: (3/10 - 3/31)
 * Ability to save ROM
 * Ability to load/open existing ROM

### Sprint 6: (3/31 - 4/14)
 * Ability to browse existing ROMs
 * Ability to edit existing ROM
 * Investigate how to export the ROM as PDF

### Sprint 7: (4/14 - 4/28)
 * Ability to customize formatting of ROM for exporting
 * Ability to export ROM as PDF

[Trello](https://trello.com/b/ceedfwG6/rom-tool)
[Bitbucket](https://bitbucket.org/cps491s20-team2/cps491s20-team2.bitbucket.io/src/master/)
[Evernote](https://www.evernote.com/Home.action#b=41486cc2-fdf9-49b0-be65-3562562fa669&ses=4&sh=1&sds=5&)

## Scrum process

### Sprint 0

Duration: 14/01/2020-22/01/2020

#### Completed Tasks:

1. Establish framework for application
2. Create Use case and use case description

#### Contributions:

1.  Daniel Hutson, 10 commits, 1 hours, contributed in Version Control Strategy, Use Case Diagram, Landing Page Updates, & Repo Cleaning/Organization
2.  Ragan Tuffey, 4 commits, 1 hours, contributed in Use Case Diagram, Documentation, & Administration  
3.  Ben Reutelshofer, 1 commits, 1 hours, contributed in Use Case Description, Sponsor Correspondence, & Scrum Master
4.  Jack McCarthy, 8 commits, 1 hours, contributed in Sprint Schedule, Technology Documentation, Repo Management




### Sprint 1

Duration: 14/01/2020-28/01/2020

#### Completed Tasks:

1. Document Branching Strategy
2. Document Version Control Strategy
3. Identify & document initial architecture
4. Design and implement intuitive html pages for the project

#### Contributions:

1.  Daniel Hutson, 7 commits, 7 hours, contributed in Version Control Strategy, Use Case Diagram, Landing Page Updates, Repo Cleaning/Organization, misc. documentation
2.  Ragan Tuffey, 4 commits, 7 hours, contributed in Use Case Diagram, Documentation, & Administration  
3.  Ben Reutelshofer, 3 commits, 7 hours, contributed in Use Case Description, Sponsor Correspondence, branching strategry, & Scrum Master
4.  Jack McCarthy, 17 commits, 15 hours, contributed in Sprint Schedule, Technology Documentation, Repo Management, File structure, & HTML pages



![Welcome Screen](imgs/welcome_screen.png "Welcome Screen")

Figure 3. - Welcome Screen for the application



![Create ROM Without Funding](imgs/create_no_funding.png "Create ROM Without Funding")

Figure 4. - ROM creation screen



![Create ROM With Funding](imgs/create_with_funding.png "Create ROM With Funding")

Figure 5. - ROM creation screen with funding provided




### Sprint 2

Duration: 28/1/2020 - 11/2/2020

#### Completed Tasks:

1. Identified security requirements
2. Ability to name ROM
3. Ability to install and run application?

#### Contributions:

1.  Daniel Hutson, 6 commits, 4 hours, contributed in Documentation
2.  Ragan Tuffey, 2 commits, 4 hours, contributed in Identified security requirements
3.  Ben Reutelshofer, 1 commits, 4 hours, contributed in ability to install and run application
4.  Jack McCarthy, 4 commits, 6 hours, contributed in Ability to name ROM



![Name Rom](imgs/name_rom.png "Name ROM Screen")

Figure 6. - Name ROM Screen for the application
